# Super duper CDS-Angular-Module


## Basic Usage

### Installation

```terminal
$ bower install --save ng-cds
```

or


```terminal
$ npm install --save ng-cds
```

Enable the module:

```javascript
var myAppModule = angular.module('MyApp', ['cdsApi']);
```

---
## Usage in controllers

### Retrieving Data

Inside your controller you can make API calls either directly to the endpoint or use a local source, such as a cache-file. If you want to make calls directly to the API, the arguments `'api'`, `authToken` and `viewId` are mandatory.

**API calls**
```javascript
// provide viewId and credentials
var viewId = '*view id from the cds url*';
var authToken = 'JWT *auth token*';

// using an endpoint
cdsApi.getContent('api', authToken, viewId).then(function(res) {
  $scope.cdsData = res.YOUR_PROPERTY.childs;
});
```

**Local sources**
```javascript
// get data from a local source
cdsApi.getContent('/path/to/file.json').then(function(res) {
  $scope.cdsData = res.YOUR_PROPERTY.childs;
});
```

**Caching**

JSON files can be cached:
* Provide a path where the cachefiles should be placed i.e. "/assets/cdsCache/cacheFile.json" instead of the 'api'-argument.
* Provide a servertoken and the viewId
* Default TTL for the cachefiles is 5 minutes. You can pass a custom integer TTL as an argument (see example)
* IMPORTANT: Don't forget to copy the file "assets/phpCache.php" from the resources folder into the caching folder (i.e. "/assets/cdsCache/")

```javascript
// api call with caching
var ttl = 10;
cdsApi.getContent('/path/toCacheFolder/cacheFile.json', serverToken, viewId, ttl).then(function(res) {
  $scope.cdsData = res.YOUR_PROPERTY.childs;
});
```

In case PHP runs on a different domain (i.e. in MAMP):
* Use an absolute URL i.e. "http://example.dev/assets/cdsCache/cacheFile.json"
* In that case you have to use an addon to enable CORS temporarily in your Browser: [LINK](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi)