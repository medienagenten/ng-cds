'use strict';

angular.
  module('cdsApi', []).
  factory('cdsApi', ['$http', '$log', '$httpParamSerializerJQLike', function ($http, $log, $httpParamSerializerJQLike) {
    $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    var APIHANDLER = function () {
      //apiHandler constructor
    };

    APIHANDLER.prototype = {
      getContent: function (source, authToken, viewId, cacheTtl) {
        if (typeof source !== 'undefined' && source !== 'api') {
          var req = {
            method: 'GET',
            url: source,
            responseType: 'json'
          };

          var createCacheFile = function () {

            var _getDomainName = (function () {
              if (source.indexOf('http://') === 0) {
                var domainname = 'http://' + source.split('/')[2];
                return domainname;
              } else if (source.indexOf('https://') === 0) {
                var domainname = 'https://' + source.split('/')[2];
                return domainname;
              } else {
                return '';
              }
            })();

            var _path = (function () {
              if (_getDomainName.length > 0) {
                var _path = source.replace(_getDomainName, '');
                _path = _path.replace('http://', '').replace('https://', '');
                return _path;
              } else {
                return source;
              }
            })();

            var _reqFilename = _path.substr(_path.lastIndexOf('/') + 1);
            var _reqPath = _path.replace(_reqFilename, '');
            var _reqData = {
              'filename': _reqFilename,
              'path': _reqPath,
              'viewId': viewId,
              'servertoken': authToken
            };

            var cacheReq = {
              url: _getDomainName + _reqPath + 'phpCache.php',
              method: "POST",
              data: $httpParamSerializerJQLike(_reqData),
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
              }
            };
            return $http(cacheReq)
              .then(function successCallback(response) {
                var type = typeof response.data;
                var _data = null;

                if (typeof response.data === 'boolean') {
                  console.warn('Could not receive data from comfortable.io!');

                } else if (typeof response.data === 'string') {
                  _data = JSON.parse(response.data);
                  console.warn('Could not receive data from comfortable.io. Using existing cachefile!');

                } else {
                  _data = response.data;

                }
                return _data;
              }, function errorCallback(response) {
                $log.log('Ooops. Something went wrong...', response);
                console.warn('Please make sure the phpCache.php file is located in ', _reqPath, 'and you have correct access rights.');
                return;
              });
          };

          return $http(req)
            .then(function successCallback(response) {
              if (typeof authToken !== 'undefined' && typeof viewId !== 'undefined') { // check if caching should be used
                var _lastModified = Date.parse(response.headers()['last-modified']) / 1000;
                var _currentTime = Date.now() / 1000 | 0;
                var _diff = (_currentTime - _lastModified) / 60; // get time difference in minutes

                var _ttl = cacheTtl || 5;

                if (_diff >= _ttl) {
                  // renew cache if outdated

                  console.info('Outdated cachefile, TTL is ' + _ttl + ' minutes. New cache file is being generated.')

                  createCacheFile(); // trigger cachefile update, for the next request

                  return response.data; // return outdated cachefile this time

                } else {
                  return response.data;
                }
              } else {
                return response.data;
              }
            })
            .catch(function (response) {
              // if a cachefile is not already present, create a new cache file
              console.info('Requested cachefile does not exist. New cache file is being generated.')
              return createCacheFile();
            });
        } else {
          if (typeof authToken == 'undefined' || typeof viewId == 'undefined') {
            throw new Error('Missing Argument: Browser AuthToken and View-ID is required');
          }

          var req = {
            method: 'GET',
            url: 'https://api.comfortable.io/v1/project/views/' + viewId + '/',
            responseType: 'json',
            withCredentials: true,

            headers: {
              'Authorization': authToken,
              'Content-Type': 'application/json;charset=utf-8'
            }
          };

          return $http(req)
            .then(function successCallback(response) {
              return response.data.data;
            }, function errorCallback(response) {
              $log.log('Error:', response);
              return;
            });
        }

      }
    };

    var cObj = new APIHANDLER();

    return {
      getContent: function (source, authToken, viewId, cacheTtl) {
        return cObj.getContent(source, authToken, viewId, cacheTtl);
      }
    };

  }]);
