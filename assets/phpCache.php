<?php

  header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
  header('Content-Type: application/json');

  /*
   * Setup Config
   */

  $serviceUrl = 'https://api.comfortable.io/v1/project/views';
  $serviceToken = $_POST["servertoken"];
  $filename = $_POST["filename"];
  $filepath = $_POST["filepath"];
  $viewId = $_POST["viewId"];

  $view = array(
            'name' => $filename,
            'id' => $viewId
          );

  $serviceCall = $serviceUrl.'/'.$view['id'].'/?token='.$serviceToken;
  $filepath = $filepath.$filename;

  $curl = curl_init($serviceCall);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $curl_response = curl_exec($curl);

  $decoded = json_decode($curl_response);

  if ($curl_response === false) {

    // error
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('An error occured during curl exec. Additioanl info: ' . var_export($info));

  } else if (json_encode($decoded->data) === 'null') {
    curl_close($curl);

    // try to get data from existing cachefile instead
    $cacheData = file_get_contents($filename, 'r');
    echo json_encode($cacheData);

  } else {
    // success, write file
    curl_close($curl);

    file_put_contents($filename, json_encode($decoded->data));

    echo json_encode($decoded->data);

  }

?>
